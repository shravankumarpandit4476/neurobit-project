import React, { useEffect, useState } from 'react';
import './App.css';
import jsonData from './schema.json';
import { Container, Row, Col, Table } from 'react-bootstrap';

const App = () => {
  const [theArray, setTheArray] = useState([]);

  // const str = jsonData.channels.toString().replace(/\B(?=(?:\d{3})+)$/g, ' ');
  console.log(jsonData.channels);
  useEffect(() => {
    setTheArray(jsonData.channels);
  }, []);

  return (
    <>
      {/* <div className='table-container'> */}
      {/* <h1>{jsonData.channels}</h1> */}
      {/* <Table striped bordered className='custom-table'>
          <thead>
            <tr>
              <th>Channel</th>
              <th>Primary Channel</th>
              <th>Default Channel</th>
              <th> </th>
            </tr>
          </thead>
          <tbody>
            {Array.from({ length: 10 }).map((_, rowIndex) => (
              <tr key={rowIndex}>
                <td>Row {rowIndex + 1}, Column 1</td>
                <td>Row {rowIndex + 1}, Column 2</td>
                <td>Row {rowIndex + 1}, Column 3</td>
                <td>Row {rowIndex + 1}, Column 4</td>
              </tr>
            ))}
          </tbody>
        </Table> */}
      {/* <h1>{data}</h1> */}
      <div>
        {/* Access the array data */}
        {/* {str.split(',').map((user) => ( */}
        {theArray.map((user) => (
          <div>
            <p>{user}</p>
          </div>
        ))}
      </div>
      {/* </div> */}
    </>
  );
};

export default App;
